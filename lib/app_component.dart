import 'dart:html';

import 'package:angular/angular.dart';

import 'app_component.template.dart' as ng;

@Component(
  selector: 'my-app',
  template: '''
  <!-- If this gets replaced with *ngIf then everything works fine. -->  
  <div *myStructural="true">
    Isn't displayed due to `myStructural`.
    <div #foo></div>
  </div>
  ''',
  directives: [MyStructuralDirective],
)
class AppComponent {
  /// This [ViewChild] is the reason this starts failing.
  @ViewChild('foo')
  DivElement foo;
}

@Directive(selector: '[myStructural]')
class MyStructuralDirective {
  final ComponentLoader _loader;
  final ViewContainerRef _viewContainer;

  MyStructuralDirective(this._viewContainer, this._loader);

  /// Whether the content of the directive should be visible.
  @Input()
  set myStructural(_) {
    _viewContainer.clear();
    _loader.loadNextTo(ng.LoadingComponentNgFactory);
  }
}

@Component(selector: 'my-loading', template: 'Loading...')
class LoadingComponent {}
