import 'package:angular/angular.dart';
import 'package:angular_issue/app_component.template.dart' as ng;

void main() {
  runApp(ng.AppComponentNgFactory);
}
