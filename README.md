# Angular 5 issue

This repo shows a bug with Ng5 when using `@ViewChild` inside a structural directive.

All the relevant code is in [lib/app_component.dart](lib/app_component.dart).


## Short explanation of the issue

My `AppComponent` has this HTML code:

```html
  <div *myStructural="true">
    Isn't displayed due to `myStructural`.
    <div #foo></div>
  </div>
```

Where `myStructural` is a structural directive, that removes the whole content of the div and replaces it with a `LoadingComponent`.

(In my real life scenario, this is used to show a loading component when the parameter passed is `false` and only
show the actual content once loaded)

Inside `AppComponent` I also have this line:

```dart
  @ViewChild('foo')
  DivElement foo;
```

And the combination of my structural directive with the `@ViewChild` throws this exception:

`EXCEPTION: Type '_ViewLoadingComponentHost0' is not a subtype of expected type '_ViewAppComponent1'.`

* * *

- Replace `myStructural` with `ngIf` works as expected
- Removing `@ViewChild` doesn't cause an exception
- Not injecting the `LoadingComponent` doesn't cause an exception